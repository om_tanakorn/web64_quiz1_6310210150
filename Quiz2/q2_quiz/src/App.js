import logo from './logo.svg';
import './App.css';

import AboutUsPage from './pages/AboutsPage';
import GradePage from './pages/GradePage';
import Header from './components/Header';


import {Routes , Route, Link} from "react-router-dom";
import GradeResult from './components/GradeResult';


function App() {
  return (
    <div className="App">
      <Header/>
      <Routes> 
          <Route path="/" element={
            <GradePage/>
          }/>
          <Route path="about" element={
            <AboutUsPage/>
          }/>
          
      </Routes>
      </div>
      
  );
}

export default App;
