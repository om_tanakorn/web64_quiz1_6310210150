
function GradeResult (props){


    return(
        <div>
            <h3> ชื่อ : {props.name} </h3>
            <h3> คุณได้คะแนน:{props.point} </h3>
            <h3> แปลว่า คุณได้เกรด: {props.grade}</h3>
        </div>
    )
}

export default GradeResult;