import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import {Link} from "react-router-dom"
import { Button } from '@mui/material';

function Header(){
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
            <Typography variant="h6"> 
                344-2xx คำนวรเกรด : 
            </Typography>
            
            
                <Link to="/">
                    <Button variant="contained">
                        <Typography variant="body1" >
                        คิดเกรด</Typography>
                        </Button>
                </Link>
                
            
            
            &nbsp;&nbsp;&nbsp;
            
                <Link to="/about">
                    <Button variant="contained"> 
                        <Typography variant="body1"> 
                        ผู้จัดทำ </Typography> 
                    </Button>
                </Link>
            
        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default Header;