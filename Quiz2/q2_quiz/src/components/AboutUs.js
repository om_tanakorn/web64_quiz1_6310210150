import { Paper,Box } from "@mui/material";



function AboutUs(props) {
    return (
    <Box xs ={8}>
        <Paper elevation={3}>
        <h2> จัดทำโดย :{props.name}</h2>
        <h3> ติดต่อได้ที่ : {props.contact} </h3>
        <h3> บ้านอยู่ :{props.home}</h3>
        <h3> เมล :{props.mail}</h3>
    </Paper>
    </Box>
    
    );
}

export default AboutUs;