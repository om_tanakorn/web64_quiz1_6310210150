import GradeResult from "../components/GradeResult";
import { Grid,Button,Container,Typography,Box } from "@mui/material";
import {useState} from "react";



function GradePage(){

    const [name , setname ] =useState(" ");
    const [point, setpoint] =useState(" ");
    const [Grade, setGrade] =useState(" ");

    function findGrade(){
        let p = parseInt(point);

        if(p > 80 ){
            setGrade("A");
        }else if(p > 70){
            setGrade("B");
        }else if(p > 60){
            setGrade("C")
        }else if(p > 50){
            setGrade("D")
        }else {
            setGrade("E");
        }

        

    }

    return(
        <Container maxWidth="lg">
        <Grid container spacing={2} sx={ {marginTop : "10px"} }>
          <Grid item xs={12}>
              <Typography variant="h4">
                  คำนวนเกรด รายวิชา 344-2xx
              </Typography>
          </Grid>
  
          <Grid item xs={8}>
            <Box sx={{textAlign : "left"}}>
                ชื่อ : <input type='text'
                              value={name}
                              onChange={ (e)=>{setname(e.target.value);} }/> <br/><br/>
                คุณได้คะแนน : <input type='int'
                              value={point}
                              onChange={ (e)=>{setpoint(e.target.value);} }/> <br/><br/>
  
              <Button variant="contained" onClick={ ()=>{ findGrade() } } >
                  หาเกรด
              </Button>
            </Box>
          </Grid>


          <Grid item xs={4}>
            { Grade != " " &&
            <div>
                ผลการคำนวน

                <GradeResult 
                    name = {name}
                    point ={point}
                    grade ={Grade}/>

            </div>
            }
          
            </Grid>

    
        </Grid>
        </Container>

    );
}

export default GradePage;