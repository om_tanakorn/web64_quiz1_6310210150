import AboutUs from "../components/AboutUs";
function AboutUsPage(){
    
    return(
        <div >
            <div>
                <h2> คะนะผู้จัดทำ เว็บนี้</h2>
                <AboutUs    name='ธนกร AKA.TheLee'
                            contact='discord'
                            home = 'หาดปากเมง'
                            mail = 'tanakorn.aphiwan@gmail.com'/>

            </div>
        </div>
    );
}

export default AboutUsPage;